import { Game, createGame, placeAnte } from "../../src/game";

export function createAntesGame() : {game:Game, human:number, bot:number} {
    const game = createGame(["Human", "Bot"])
    const human = game.players.indexOf("Human")
    const bot = game.players.indexOf("Bot")
    placeAnte(game, human, 1)
    placeAnte(game, bot, 1)
    return {game, human, bot}
}