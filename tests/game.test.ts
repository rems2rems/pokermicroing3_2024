import { createGame, placeAnte, play } from "../src/game";
import { createAntesGame } from "./fixtures/games";

describe('Game', () => {
    beforeEach(() => {});

    it('must manage antes ', () => {

        const game = createGame(["Human", "Bot"])
        // console.log(game);
        
        const human = game.players.indexOf("Human")
        const bot = game.players.indexOf("Bot")
        expect(game.stage).toBe("Antes");
        expect(game.currentPlayer).toBe(human);
        expect(game.stacks[human]).toBe(100);
        expect(game.stacks[bot]).toBe(100);
        placeAnte(game, human, 1)
        expect(game.currentPlayer).toBe(bot);
        placeAnte(game, bot, 1)
        expect(game.stage).toBe("Bets");
    });

    it('must manage bets ', () => {

        const {game, human, bot} = createAntesGame()
        // console.log(game);
        expect(game.stage).toBe("Bets");
        expect(game.currentPlayer).toBe(human);
        expect(game.stacks[human]).toBe(99);
        expect(game.stacks[bot]).toBe(99);
        play(game, human, "bet",5)
        expect(game.currentPlayer).toBe(bot);
        play(game, bot, "call",5)
        expect(game.stage).toBe("Showdown");
    });
});