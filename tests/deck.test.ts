import { Card, bySuitAndRank, eq as equals, toString } from "../src/card";
import { createDeck } from "../src/deck";

describe('Deck', () => {
    let deck : Card[] | null =null

    beforeEach(() => {
        deck = createDeck();
    });

    it('must display a card', () => {
        let card : Card = {suit:"spade", rank: 9}
        expect(toString(card)).toBe("9s");
        card = {suit:"spade", rank: 10}
        expect(toString(card)).toBe("Ts");
        card = {suit:"spade", rank: 11}
        expect(toString(card)).toBe("Js");
        card = {suit:"spade", rank: 12}
        expect(toString(card)).toBe("Qs");
        card = {suit:"spade", rank: 13}
        expect(toString(card)).toBe("Ks");
        card = {suit:"spade", rank: 14}
        expect(toString(card)).toBe("As");

        card = {suit:"heart", rank: 9}
        expect(toString(card)).toBe("9h");
        card = {suit:"heart", rank: 10}
        expect(toString(card)).toBe("Th");
        card = {suit:"heart", rank: 11}
        expect(toString(card)).toBe("Jh");
        card = {suit:"heart", rank: 12}
        expect(toString(card)).toBe("Qh");
        card = {suit:"heart", rank: 13}
        expect(toString(card)).toBe("Kh");
        card = {suit:"heart", rank: 14}
        expect(toString(card)).toBe("Ah");
    });
    it('must have 12 unique cards', () => {
        expect(deck!.length).toBe(12);
        const seenCards = []
        for(let card1 of deck!){  
            expect([9,10,11,12,13,14]).toContain(card1.rank)
            expect(["spade", "heart"]).toContain(card1.suit)          
            for(let card2 of seenCards){                
                const areSame = card1.rank === card2.rank && card1.suit === card2.suit
                expect(areSame).not.toBe(true)
            }
            seenCards.push(card1)
        }
    });
    it('must be shuffled', () => {
        const sortedDeck = createDeck();
        sortedDeck.sort(bySuitAndRank)
        let entropy = 0
        for(let i = 0; i < deck!.length; i++){            
            const idx = deck!.findIndex(card => equals(card, sortedDeck[i]))
            let distance = Math.abs(idx-i)
            entropy += distance            
        }
        entropy /= deck!.length
        
        expect(entropy).toBeGreaterThan(2)
    })
});