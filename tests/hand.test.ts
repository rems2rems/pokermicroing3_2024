import { Card, rankOf } from "../src/card";
import { makeHand, toString } from "../src/hand";

describe('Hand', () => {
    beforeEach(() => {});

    it('must parse a high-card hand', () => {
        const cards = [
            {suit:"spade", rank: 9},
            {suit:"spade", rank: 10},
            {suit:"heart", rank: 14}
        ]
        const hand = makeHand(cards as Card[])
        expect(hand).toBeTruthy()
        expect(hand.category).toBe("HighCard")
        expect(hand.rank).toBe(rankOf("A"))
        expect(hand.cards).toBeInstanceOf(Array)
        expect(hand.cards.length).toBe(3)
        expect(hand.cards[0].rank).toEqual(rankOf("A"))
        expect(hand.cards[1].rank).toEqual(rankOf("T"))
        expect(hand.cards[2].rank).toEqual(rankOf("9"))
        expect(toString(hand)).toBe("HighCard, rank A (Ah Ts 9s)")
    });

    it('must parse a pair hand', () => {
        const cards = [
            {suit:"spade", rank: 9},
            {suit:"spade", rank: 10},
            {suit:"heart", rank: 10}
        ]
        const hand = makeHand(cards as Card[])
        expect(hand).toBeTruthy()
        expect(hand.category).toBe("Pair")
        expect(hand.rank).toBe(rankOf("J"))
        expect(hand.cards).toBeInstanceOf(Array)
        expect(hand.cards.length).toBe(3)
        // expect(hand.cards[0].rank).toEqual(rankOf("J"))
        // expect(hand.cards[1].rank).toEqual(rankOf("J"))
        // expect(hand.cards[2].rank).toEqual(rankOf("9"))
        // expect(toString(hand)).toBe("Pair, rank J (Js Jh 9s)")
    });
});