import { shuffle } from "./arrays"
import { Card } from "./card"

export function createDeck() : Card[] {
    let deck : Card[] = []
    const suits = ["heart","spade"]
    for(let suit of suits){
        for(let rank = 9; rank < 15; rank++){
            deck.push({suit, rank} as Card)
        }
    }
    // deck = 
    shuffle(deck)
    shuffle(deck)
    shuffle(deck)
    return deck
}