export type Card = {
  rank: Rank;
  suit: Suit;
};

export type Rank = 9 | 10 | 11 | 12 | 13 | 14;
export type Suit = "spade" | "heart";

const chars = ["9", "T", "J", "Q", "K", "A"];

export function toString(card: Card): string {
  return `${faceOf(card)}${card.suit[0].toLowerCase()}`;
}

export function rankOf(str: string) {
  return chars.indexOf(str[0]) + 9;
}

export function bySuitAndRank(card1: Card, card2: Card): number {
  if (card1.suit < card2.suit) return -1;
  if (card1.suit > card2.suit) return 1;
  if (card1.rank < card2.rank) return -1;
  if (card1.rank > card2.rank) return 1;
  return 0;
}

export function byRankAndSuit(card1: Card, card2: Card): number {
  if (card1.rank < card2.rank) return -1;
  if (card1.rank > card2.rank) return 1;
  if (card1.suit < card2.suit) return -1;
  if (card1.suit > card2.suit) return 1;
  return 0;
}

export function eq(card1: Card, card2: Card): boolean {
  return card1.rank === card2.rank && card1.suit === card2.suit;
}

export function faceOf(card:Card):string{
    return chars[card.rank - 9]
}

export function faceOfRank(rank:number):string{
    return chars[rank - 9]
}
