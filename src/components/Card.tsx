import { Card,toString } from "../card";

export function Card({ card }: {card:Card|null}) {
    let border = "info"
    let bg = "info"
    if(card?.suit === "heart"){
        border = "danger"
        bg="light"
    }
    if(card?.suit === "spade"){
        border = "black"
        bg="light"
    }
  return (
    <div className={`card box has-background-${bg}`}>
      <div className={`value has-text-${border}-dark`}>{card!=null?toString(card):"?"}</div>
    </div>
  );
}
