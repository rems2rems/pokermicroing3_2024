import { Game } from "../game";
import { Card } from "./Card";

export function Game({
  human,
  game,
  bot,
  onPlaceAnte,
}: {
  human: number;
  bot: number;
  game: Game;
  onPlaceAnte: () => void;
}) {
  return (
    <div className="game">
      {game.cards && (
        <div className="hands columns">
          <div className="human column is-5">
            <div className="columns">Your hand:</div>
            <div className="cards columns">
              {game.cards[human].map((card, idx) => (
                <div className="column">
                  <Card key={idx} card={card} />
                </div>
              ))}
            </div>
          </div>
          <div className="bot column is-offset-2 is-5">
            <div className="columns">Bot hand:</div>
            <div className="cards columns">
              {[null, null, null].map((card, idx) => (
                <div className="column">
                  <Card key={idx} card={card} />
                </div>
              ))}
            </div>
          </div>
        </div>
      )}
      <div className="buttons is-justify-content-center is-size-5">
        {game.stage == "Antes" && (
          <button className="ante " onClick={onPlaceAnte}>
            Place ante
          </button>
        )}
        {game.stage == "Bets" && (
          <>
            <div className="columns">
              <div className="column-one-third">
                <button className="fold mx-3">Fold</button>
                <button className="call mx-3">Call</button>
                <button className="bet mx-3">Bet</button>
              </div>
            </div>
          </>
        )}
        {game.stage == "Showdown" && (
          <button className="show">Show cards</button>
        )}
      </div>
    </div>
  );
}
