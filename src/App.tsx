import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { Game } from "./components/Game";
import { createGame, placeAnte } from "./game";

function App() {
  const [game, setGame] = useState(createGame(["Human", "Bot"]));
  const [human, setHuman] = useState(0);
  const [bot, setBot] = useState(1);
  return (
    <>
      <div className="game block mt-6">
        <div className="stacks columns">
          <div className="stack column is-5">
            <div className="your">Your stack</div>
            <div className="amount">100</div>
          </div>
          <div className="spacer column is-offset-2"></div>
          <div className="stack column is-5">
            <div className="theirs">Bot stack</div>
            <div className="amount">100</div>
          </div>
        </div>
        <Game
          game={game}
          human={human}
          bot={bot}
          onPlaceAnte={() => {
            placeAnte(game, human, 1);
            placeAnte(game, bot, 1);
            setGame({ ...game });
          }}
        />
      </div>
    </>
  );
}

export default App;
