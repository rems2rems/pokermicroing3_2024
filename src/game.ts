import { Card } from "./card"
import { createDeck } from "./deck"

export function createGame(players:string[]) : Game {
    return {
        players: players,
        stage: "Antes",
        currentPlayer: 0,
        antes: [],
        bets: [],
        winners: [],
        stacks: [100,100],
        deck : createDeck(),
        cards: null
    }
}

export type Stage = "Antes" | "Bets" | "Showdown" | "Over"

export type Game = {
    players: string[],
    stage: Stage,
    currentPlayer: number,
    antes: number[],
    bets: number[],
    winners : number[],
    stacks: number[],
    cards: Card[][] | null,
    deck: Card[]
}

export function placeAnte(game:Game, player:number,amount:number = 1) {
    game.antes[player] = amount
    game.stacks[player] -= amount
    if(game.antes.length === 2 && game.antes.every((ante) => ante !== 0 && (ante - game.antes[0]) === 0) ) {
        game.stage = "Bets"
        game.cards = []
        game.cards[0] = game.deck.slice(0, 3)
        game.cards[1] = game.deck.slice(0, 3)
    }
    nextPlayer(game)
}

export function nextPlayer(game:Game) {
    game.currentPlayer = (game.currentPlayer + 1) % game.players.length
}

export function play(game:Game, player:number, action:string, amount:number = 0) {
    game.bets[player] = amount
    game.stacks[player] -= amount
    if(game.bets.length == 2 && game.bets.every((bet) => (bet - game.bets[0]) === 0) ) {
        game.stage = "Showdown"
    }
    nextPlayer(game)
}