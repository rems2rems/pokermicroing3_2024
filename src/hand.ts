import { uniqueArray } from "./arrays"
import { Card,Rank,byRankAndSuit,faceOf,faceOfRank,toString as toStringCard } from "./card"

export function makeHand(cards:Card[]) : Hand {
    const isPair = uniqueArray(cards.map(card => card.rank)).length === 2
    const sortedCards = [...cards]
    sortedCards.sort(byRankAndSuit)
    sortedCards.reverse()
    if(isPair){
        return {category:"Pair", rank:11, cards:sortedCards}
    }
    return {category:"HighCard", rank:14, cards:sortedCards}
}

export type Category = "StraightFlush" | "Straight" | "Flush" | "Pair" | "HighCard"

export type Hand = {
    category : Category,
    rank : Rank,
    cards : Card[]
}

export function toString(hand:Hand) : string {
    return `${hand.category}, rank ${faceOfRank(hand.rank)} (${hand.cards.map(card => toStringCard(card)).join(" ")})`
}
